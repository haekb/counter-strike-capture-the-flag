// Capture the Flag Game Mode
// Based off of Valve's Control Point example.
// ------------------
// History
//
//
//  2/10/13 JWB - Solid, working game mode.
//
//	2/8/13 JWB - Started code.
//
//

// How we're going to do this...

// This class only deals with the instance of a flag.
// Thus we only need to worry about the flag's state in here
// While we handle the scoring in ctf_base.nut

// These are some of the events we need to handle

// OnPickup - it will... 
// -Check to see if player picking up is on opposing team as flag
// -Send out an alert (Sound entity) 
// -Glow 
// -Disable the flag carrier's weapon access

// OnDrop - it will... 
// -Send out an alert (Sound entity)
// -Glow 
// -Enable the previous flag carrier's weapon access

// OnRetrieve - it will...
// -Check to see if player picking up is on same team as flag
// -Send out an alert (Sound entity)
// -Unglow
// -Send flag back to cap point

// OnCapture - it will...
// -Check to see if Capture Team has flag (They can't cap if they don't have their own flag!)
// -Send out an alert (Sound entity)
// -Unglow
// -Enable the previous flag carrier's weapon access
// -Send flag back to cap point


SHOW_CONSOLE_MESSAGES <- false; 		// Debug Messages

printl("==================================================");
printl("Jake's Capture the flag! - Script Start");
printl("==================================================");

// these match the enties defined in the EntityGroup entries in the logic_script entity in Hammer
m_PickupZone <- EntityGroup[0];
m_Flag_Model <- EntityGroup[1];
m_Flag_Model_Flag <- EntityGroup[2];
m_Flag_Respawn <- EntityGroup[3];


m_nTeam <- 0;

if("FlagCarrier" in getroottable())
	printl("Found FlagCarrier on script re-run!");
	//OnRoundEnded();
	
// Clear flag carrier, wish there was a better way to do it but doesn't seem to be...
EntFire("flagman_T","AddOutput","targetname ",0);	    	// Reset the flagman_T name
EntFire("flagman_CT","AddOutput","targetname ",0);	    	// Reset the flagman_CT name

// These are for Checks in the Think() Function
// We only want those checks to run once.
m_bCheckHealthOnce <- false;
m_bCheckTeamOnce <- false;


// Empty FlagCarrier 
FlagCarrier <- null;
// Code scope is weird, redesign later. But for now we use this.
FlagCarrier_Name <- null; 

// Sets the specific player holding the flag
function setPlayer(nTeam)
{
	if(SHOW_CONSOLE_MESSAGES)
		printl("setPlayer("+nTeam+") - Start");
	// Just in case...
	FlagCarrier <- null;
	
	// Figure out what team has the flag
	if(nTeam == 2)
	{
		FlagCarrier <- Entities.FindByName(null, "flagman_T");
		m_nTeam = nTeam;
	}
	else if(nTeam == 3)
	{
		FlagCarrier <- Entities.FindByName(null, "flagman_CT");
		m_nTeam = nTeam;
	}
	FlagCarrier_Name <- FlagCarrier.GetName();
	
	// Dev messages, if you want to see these set SHOW_CONSOLE_MESSAGES to true.
	if(SHOW_CONSOLE_MESSAGES)
	{
		printl("Set Player ran");
		printl("SetPlayer.FlagCarrier = "+FlagCarrier.GetName() );
		printl("SetPlayer.nTeam = "+nTeam);
	}
	if(SHOW_CONSOLE_MESSAGES)
		printl("setPlayer("+nTeam+") - End");
}

// Flag small zone which when interacted with will cause the player to pick up the flag
function touchZone(nTeam)
{
	if(SHOW_CONSOLE_MESSAGES)
		printl("touchZone("+nTeam+") - Start");
	
	// Allow checks, since they just picked the flag up!
	// Errors could occur and shiz
	m_bCheckTeamOnce = true;
	m_bCheckHealthOnce = true;
	
	// Colour Guide
	// CT
	// 123,170,190
	// T
	// 180,160,90
	
	
	// Figure out what Team we're dealing with, then set the colour!
	if(nTeam == 2)
	{
		EntFire(m_Flag_Model.GetName(),"glowcolorredvalue",123,0);
		EntFire(m_Flag_Model.GetName(),"glowcolorgreenvalue",170,0);
		EntFire(m_Flag_Model.GetName(),"glowcolorbluevalue",190,0);
	}
	else if(nTeam == 3)
	{
		EntFire(m_Flag_Model.GetName(),"glowcolorredvalue",180,0);
		EntFire(m_Flag_Model.GetName(),"glowcolorgreenvalue",160,0);
		EntFire(m_Flag_Model.GetName(),"glowcolorbluevalue",90,0);
	}
	
	EntFire(m_Flag_Model.GetName(),"SetGlowEnabled","",0);						// Enable that lovely glow
	EntFire(m_Flag_Model.GetName(),"setparent", FlagCarrier.GetName() , 0 )		// Syncs the flag's movement with the carrier
	EntFire(m_Flag_Model.GetName(),"SetParentAttachment","grenade2",0);		    // Places flag on back
	
	// Dev messages, if you want to see these set SHOW_CONSOLE_MESSAGES to true.
	if(SHOW_CONSOLE_MESSAGES)
	{
		printl("touchZone ran!");
		printl("touchZone.m_Flag_Model = "+m_Flag_Model.GetName());
		printl("touchZone.FlagCarrier = "+FlagCarrier);
	}
	
	if(SHOW_CONSOLE_MESSAGES)
		printl("touchZone("+nTeam+") - End");
}

function onCapture(nTeam)
{
	if(SHOW_CONSOLE_MESSAGES)
		printl("OnCapture("+nTeam+") - Start");
	
	
	EntFire(m_Flag_Model.GetName(),"SetGlowDisabled","",0);			// Turn off the glow...I didn't like it anyways...
	
	EntFire(m_Flag_Model.GetName(),"turnoff", "" , 0 )				// Turns off the pole model
	EntFire(m_Flag_Model_Flag.GetName(),"turnoff", "" , 0 )			// Turns off the flag model
	
	EntFire(m_Flag_Model.GetName(),"clearparent", "", 0 );			// Unparent the flag
	EntFire(FlagCarrier_Name,"AddOutput","targetname ",0);	    	// Reset the flagman_T name

	m_Flag_Model.SetOrigin(m_Flag_Respawn.GetOrigin());				// Reset flag
	//m_Flag_Model.SetAngles(0,0,0);						     	// Set the flag upright!
	m_Flag_Model.SetForwardVector(Vector(1,0,0));					// NO, this sets the flag upright!!
	EntFire(m_PickupZone.GetName(),"Enable","",0);					// Reenable pickup!
	FlagCarrier = null;												// Remove the FlagCarrier	
	FlagCarrier_Name = null;
	
	if(SHOW_CONSOLE_MESSAGES)
		printl("OnCapture("+nTeam+") - End");
}

// This runs 3 seconds after OnCapture()
// Because the flag wasn't returning to the right spot..
function onCaptureCleanUp()
{
	// Dev messages, if you want to see these set SHOW_CONSOLE_MESSAGES to true.
	if(SHOW_CONSOLE_MESSAGES)
		printl("onCaptureCleanUp - Start");

	m_Flag_Model.SetOrigin(m_Flag_Respawn.GetOrigin());				// Reset flag
	m_Flag_Model.SetAngles(0,0,0);						     		// Set the flag upright!

	
	EntFire(m_Flag_Model.GetName(),"turnon", "" , 0 )				// Turns on the pole model
	EntFire(m_Flag_Model_Flag.GetName(),"turnon", "" , 0 )			// Turns on the flag model
	
				
	// Dev messages, if you want to see these set SHOW_CONSOLE_MESSAGES to true.
	if(SHOW_CONSOLE_MESSAGES)
		printl("onCaptureCleanUp - End");
}


function Think()
{		
	// If there is a flag carrier
	if(FlagCarrier != null)
	{

		// This will spam 
		if(SHOW_CONSOLE_MESSAGES)
		{
			//printl("Think.m_nTeam = "+m_nTeam);
			//printl("Think.FlagCarrier.GetTeam() = "+FlagCarrier.GetTeam());
		}	
		
		// If the flag carrier is dead..and run only once
		if(FlagCarrier.GetHealth() == 0 && m_bCheckHealthOnce == true)
		{
			// Dev messages, if you want to see these set SHOW_CONSOLE_MESSAGES to true.
			if(SHOW_CONSOLE_MESSAGES)
				printl("Think() - Flag Carrier is Dead - Start");

			EntFire(m_Flag_Model.GetName(),"clearparent", "", 0 );		// Unparent the flag
			EntFire(FlagCarrier_Name,"AddOutput","targetname ",0);	// Reset the flagman_T name
			EntFire(m_PickupZone.GetName(),"Enable","",0);				// Reenable pickup!
			m_Flag_Model.SetForwardVector(Vector(1,0,0));				// NO, this sets the flag upright!!
			m_Flag_Model.SetOrigin(Vector(0,0,-100));					// Lower the flag to the ABOUT ground level

			FlagCarrier = null;											// Remove the FlagCarrier					
			FlagCarrier_Name = null;
			m_bCheckHealthOnce = false;									// Only runs once!
			
			
			if(SHOW_CONSOLE_MESSAGES)
				printl("Think() - Flag Carrier is Dead - End");
		}
		
		// If the variable FlagCarrier exists
		if("FlagCarrier" in getroottable())
		{
			// If the flag carrier changed teams..and run only once
			if(FlagCarrier.GetTeam() != m_nTeam)
			{
				if(SHOW_CONSOLE_MESSAGES)
					printl("Think() - Team Change - Start");
			
				EntFire(m_Flag_Model.GetName(),"clearparent", "", 0 );		// Unparent the flag
				EntFire(FlagCarrier_Name,"AddOutput","targetname ",0);   	// Reset the flagman_T name
				EntFire(m_PickupZone.GetName(),"Enable","",0);				// Reenable pickup!
				FlagCarrier = null;											// Remove the FlagCarrier	
				m_bCheckTeamOnce = false;									// Only runs once!		
				
							if(SHOW_CONSOLE_MESSAGES)
					printl("Think() - Team Change - End");
			}
		}
	}
}
  


function OnRoundEnded()
{
	if(SHOW_CONSOLE_MESSAGES)
	{
		printl("OnRoundEnded() - Start");
		printl(FlagCarrier_Name);
		printl(FlagCarrier);
	}
	// This never runs because the script re-runs thus FlagCarrier gets nulled
	if(FlagCarrier != null)
	{
		EntFire(m_Flag_Model.GetName(),"clearparent", "", 0 );		// Unparent the flag
		EntFire(FlagCarrier_Name,"AddOutput","targetname ",0);	    // Reset the flagman_T name
		EntFire(m_PickupZone.GetName(),"Enable","",0);				// Reenable pickup!
		FlagCarrier = null;											// Remove the FlagCarrier	
		FlagCarrier_Name = null;
		if(SHOW_CONSOLE_MESSAGES) 
		{
			printl("FLAG CARRIER HAS BEEN NULLED.");
		}
	}
	if(SHOW_CONSOLE_MESSAGES)
		printl("OnRoundEnded() - End");
}







/////////////////////////////////////////
