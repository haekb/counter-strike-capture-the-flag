// Capture the Flag Game Mode
// Based off of Valve's Control Point example.
// ------------------
// History
//
//	2/8/13 JWB - Started code
//
//
//
//


// This script handles the overall state and scoring of the game mode


// It handles these events...

// OnCapture - it will...
// -Add one point to the scoring team
// -Check to see if point cap is reached


CAP_LIMIT <- 3; 		// Number of points required for game to finish
POINTS_PER_CAP <- 1; 	// Number of points you get for a cap

FlagCarrier_T <- null
FlagCarrier_CT <- null;

// ---

function onCapture(nTeam)
{
	if(nTeam == 2)
	{
		//EntFire( "@game_round_end","EndRound_TerroristsWin", "7", 0 );
		EntFire( "@game_score","AddScoreTerrorist", "", 0 );
	}
	else if(nTeam == 3)
	{
		EntFire( "@game_score","AddScoreCT", "", 0 );
	}
}